# Training Resources for Programming

This document is a collection of training resources for programming. It may include online courses, workshops, apps, and so on on any number of programming languages. It is intended to grow, therefore, please add any resources you may have. 

|Title and Link | Description | Participants | Additional Information |
|---------------|-------------|--------------|------------------------|
|[Sololearn](https://www.sololearn.com/home) | Browser and App courses for multiple programming languages; paid (pro) and free versions | Nikki - Python Core (Pro - paid; in progress) | Free version has it's limits, more practical examples and tools with Pro ; options to test out of modules to skip ahead; covers the basics, but a little more in depth than mimo or code academy; has a large community with discussion boards (in my experience -Nikki) |
[Codecademy](https://www.codecademy.com/) | Browser and app courses for multiple programming languages; paid and free versions | Nikki - some Python basics | somewhat basic (but this may have changed); easy to use |
[Mimo](https://getmimo.com/) | App courses for multiple programming languages; paid and free versions | Nikki has tried it | fairly basic |
| [Python 3 Programming Coursera Specialization](https://www.coursera.org/specializations/python-3-programming?specialization=python) | consists of five online courses in Python, which cover the basics and end in a hands on project. Can audit for free or pay to receive a certificate. | Nikki (completed) | Good way to learn the basics as well as some more in-depth skills. The final project also includes Jupyter Notebooks, which is very helpful |
| [Invent with Python](https://inventwithpython.com/) | Collection of free books and videos, convering basic to intermediate Python | Jonathan | "Automate the Boring Stuff" is often reccomended for people starting out, but he also has "Beyond the Basic Stuff" and "The Big Book of Small Python Projects", which are pretty decent approaches to higher level concepts. 
| [PY4e](https://www.py4e.com/), [WA4e](https://www.wa4e.com/), [PG4e](https://www.pg4e.com/), [DJ4e](https://www.dj4e.com/) | Free courses covering basics in Python, PHP, SQL and Django. | Jonathan | Self paced courses from my old UM professor. Mostly introductory stuff. A very talented programmer helped write the quiz module. |
